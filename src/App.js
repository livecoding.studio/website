import React from 'react';
import './App.css'; 
import { MainBanner } from './components/MainBanner' 
import { Content } from './components/Content' 
import { LanguageBlock } from './components/LanguageBlock' 
import { ProfileBlock } from './components/ProfileBlock' 
import { GalleryBlock } from './components/GalleryBlock' 

function App() {
  return (
    <div className="wrapper">
      <MainBanner></MainBanner>
      <div className="main-content">
        <Content full={true} disableBar={true} disableSpacing={true}>
          <h2>Format</h2>
          <h5>Classtime</h5>
          <p>
          Two-hour group workshops in each language showcasing installation, first steps, basic syntax and workflow, and performance and artistic techniques in an encouraging environment. Everything to get you started up and confidently exploring on your own!
          </p>
          <h5>Office hours</h5>
          <p>
          Office hours with each instructor allowing each participant to explore and ask specific questions or request help with the languages that interest them.</p>
          <h5>Algorave workshop</h5>
          <p>
          A workshop covering the best practices and techniques for performing both in-person and livestreaming remotely.
          </p>
          <h5>Showcase</h5>
          <p>
          It all culminates in a Student Showcase, an Algorave where participants team up to perform for the first time for each other and friends, all with instructor support.
          </p>
        </Content>
        <Content disableBar={true} disableSpacing={true} full={true}>
          <h2>Coding Visuals</h2>
        </Content>
        <Content disableBar={true} disableSpacing={true}>
          <LanguageBlock image="/static/photos/language_hydra.png">
              <h5>Hydra</h5>
              <p>
              Hydra is a live-coding environment for creating visuals using JavaScript inspired by modular analog video synthesis. Multiple visual sources can be combined, transformed, and fed back onto themselves, building shifting canvases of light and color.
              <br/>
              <a target="_blank" href="https://hydra.ojack.xyz/">Hydra site</a></p>
          </LanguageBlock>
        </Content>
        <Content disableBar={true} disableSpacing={true}>
          <LanguageBlock link="https://newart.city/show/frag" image="/static/photos/language_glsl.png">
              <h5>GLSL</h5>
              <p>
              GLSL (Open Graphics Library Shader Language) is a C-like programming language that allows you to code powerful graphics programs called shaders directly on your computer’s graphics card. GLSL usually involves using math operations to describe color and shape, but because of the power of shaders, you only need to understand a little to get stunning results.
              <br/>
              <a target="_blank" href="https://www.khronos.org/opengl/wiki/Core_Language_(GLSL)">GLSL info</a></p>
          </LanguageBlock>
        </Content>
        <Content disableBar={true} disableSpacing={true} full={true}>
          <h2>Coding Audio</h2>
        </Content>
        <Content disableBar={true}>
          <LanguageBlock image="/static/photos/language_tidal.png">
              <h5>TidalCycles</h5>
              <p>
              TidalCycles is a free and open-source tool that allows you to create and manipulate musical patterns with code. Patterns and sequences can be nested, rearranged, and modified on the fly, enabling unique methods of composition and performance.
              <br/>
              <a target="_blank" href="https://tidalcycles.org/index.php/Welcome">TidalCycles site</a></p>
          </LanguageBlock>
        </Content>
        <Content disableBar={true}>
          <LanguageBlock image="/static/photos/language_orca.png">
              <h5>Orca</h5>
              <p>
              Orca is a two-dimensional esoteric programming language for sequencing and composing music. Almost reminiscent of John Conway's Game of Life, Dwarf Fortress, or kinetic ASCII art, ORCA allows a unique approach to music making and programming, building rhythms and melodies out of colliding alphanumeric characters. 
              <br/>
              <a target="_blank" href="https://github.com/hundredrabbits/Orca">Orca site</a></p>
          </LanguageBlock>
        </Content>
        <Content full={true}>
          <h2>Instructors</h2> 
          <ProfileBlock image="/static/photos/zach_profile.jpg">
              <h5>Teaching Hydra</h5>
              <p>
              <a target="_blank" href="https://zachkrall.com/">Zach Krall</a> is a computational artist and designer in New York City. He received his MFA in Design and Technology from Parsons School of Design and is currently teaching in the MS Data Visualization department. With Hydra, Zach uses feedback, color, video, and reactivity, to build visual canvases that leverage both digital and physical space for maximum effect. 
              </p>
          </ProfileBlock>
          <ProfileBlock image="/static/photos/char_profile.jpg">
              <h5>Teaching GLSL</h5>
              <p>
              <a target="_blank" href="http://charstiles.com/">Char Stiles</a> is a multidisciplinary digital artist who uses emerging technologies as tools for expression. A graduate of Carnegie Mellon, she was creative-in-residence at the Frank-Ratchye STUDIO for Creative Inquiry. Her shader and raymarching workshops teach students, both new coders and veteran engineers, how to utilize geometry and light to construct virtual architectures for an audience. </p>
          </ProfileBlock>
          <ProfileBlock image="/static/photos/dan_profile.jpg">
              <h5>Teaching Tidal Cycles</h5>
              <p>
              <a target="_blank" href="https://dan.dog/">Dan Gorelick</a> is a Brooklyn-based engineer, creative coder, and musician. A graduate of Boston University, a hackNY fellow, a member of the Fall 2016 cohort at the School for Poetic Computation. An accomplished keyboardist and cellist, Dan merges live coding with traditional musical performance, exploring new forms of improvisation and composition.</p>
          </ProfileBlock>
          <ProfileBlock image="/static/photos/max_profile.jpg">
              <h5>Teaching Orca</h5>
              <p>
              <a target="_blank" href="https://www.maxwellneelycohen.com/">Maxwell Neely-Cohen</a> is a writer and musician based in New York City. His experiments with technology have been acclaimed by The New York Times Magazine, Electric Literature, The Financial Times, and Google. In 2019 he was an artist-in-residence at Pioneer Works and CultureHub. He uses and teaches Orca in the hopes of discovering novel approaches in both music and computation.</p>
          </ProfileBlock>
          <h5 style={{marginTop: '2rem'}}>Algoraves</h5>
          <p><a target="_blank" href="https://youtu.be/Nshk-LGsJLM">Watch Char and Dan perform a remotely with GLSL and TidalCycles</a></p> 
          <p><a target="_blank" href="https://www.youtube.com/watch?v=_vbz5BJpg3g">Watch Zach and Max perform with Hydra and Orca</a></p> 
        </Content>
        <div style={{display: 'none'}}>
          <Content 
            full={true} 
            fullBleed={true}
          >
            <h2>Student Work</h2>
            <GalleryBlock
            blocks={[
              {
                image: "/static/photos/00_max_zach.gif",
                text: 'Example of Hydra work'
              },
              {
                image: "/static/photos/00_max_zach.gif",
                text: 'Example of Orca work'
              },
              {
                image: "/static/photos/00_max_zach.gif",
                text: 'Example of TidalCycles work'
              },
              {
                image: "/static/photos/00_max_zach.gif",
                text: 'Example of GLSL work'
              },
              {
                image: "/static/photos/00_max_zach.gif",
                text: 'Example of Hydra work'
              },
              {
                image: "/static/photos/00_max_zach.gif",
                text: 'Example of Orca work'
              },
              {
                image: "/static/photos/00_max_zach.gif",
                text: 'Example of TidalCycles work'
              },
              {
                image: "/static/photos/00_max_zach.gif",
                text: 'Example of GLSL work'
              }
            ]}
            ></GalleryBlock>
          </Content>
        </div>
        <Content disableSpacing={true} full={true}>
          <h2 id="contact">Contact</h2>
          <h4>For inquiries: <strong>hi@livecode.studio</strong></h4>
        </Content>
        <Content disableBar={true}>
          <h2>Press</h2>
          <p><a target="_blank" href="https://www.nytimes.com/2019/10/04/style/live-code-music.html">New York Times: “That Music You’re Dancing To? It’s Code”</a></p>
          <p><a target="_blank" href="https://www.ft.com/content/0b64ec04-c283-11e9-ae6e-a26d1d0455f4">Financial Times: “Electronic dance music and ‘algorave’ — how live coding got cool”</a></p>
        </Content>
        <Content disableBar={true}>
          <h2>Links</h2>
          <p><a target="_blank" href="#">Max workshop for Babycastles Academy</a></p>
          <p><a target="_blank" href="#">Dan workshop for Babycastles Academy</a></p>
          <p><a target="_blank" href="#">Char workshop for Mutek SF</a></p>
        </Content>
      </div>
      <Content 
          full={true} 
      > 
        <h3 style={{fontWeight: 'bold'}}>Livecode.studio 2020</h3>
      </Content>
    </div>
  );
}

export default App;
