import React from 'react';
import './ProfileBlock.scss'

export function ProfileBlock(props) {
    return (
        <div className='profile'>
            <div className='profile-image'>
                <img src={props.image} alt="image" />
            </div>
            <div className='profile-text'>
                {props.children}
            </div>
        </div>
    )
}