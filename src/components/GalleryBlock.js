import React from 'react';
import './GalleryBlock.scss'

export function GalleryBlock(props) {
    return (
        <div className='gallery'>
            {
                props.blocks.map((block, index) => {
                    return (
                        <div className='gallery-block' key={index}>    
                            <div 
                                className='gallery-image'
                                style={{
                                    backgroundImage: `url(${block.image})`
                                }}
                            >
                            </div>
                            <div className='gallery-text'>
                                {block.text}
                            </div>
                        </div>
                    )
                })
            }      
        </div>
    )
}