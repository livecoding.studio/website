import React from 'react';
import './Content.scss'

export function Content(props) {

    const classes = `content-wrapper 
        ${props.full ? 'full' : ''} 
        ${props.fullBleed ? 'full-bleed' : ''} 
        ${props.disableBar ? 'disable-bar' : ''}
        ${props.disableSpacing ? 'disable-spacing' : ''}
        ${props.class ? props.class : ''}
        `
    return (
        <div className={classes} style={props.style}>
            <div className='content'>
                <div>
                    {props.children}
                </div>
            </div>
        </div>
    )
}