import React, { useEffect, useState }  from 'react';
import './MainBanner.scss'
import { Content } from './Content';


export function MainBanner(props) {
    const [offsetX, setOffsetX] = useState(window.innerWidth >= 800 ? window.innerWidth/4 : 0);
    const [offsetY, setOffsetY] = useState(window.innerWidth >= 800 ? window.innerHeight/6 : 0);
    
    function getX() {
        if (window.innerWidth > 1200) {
            const width = (1200)
            const xOffset = (window.innerWidth - width)
            const livecodeImageSize = 500
            return (Math.random() * ((width) - xOffset))
        } else {
            const width = (window.innerWidth*0.85)
            // const xOffset = (window.innerWidth - width)/2
            const livecodeImageSize = 500
            return (Math.random() * ((width - livecodeImageSize)))
        }
    } 

    function moveImage() {
        return setTimeout(() => {
            if (window.innerWidth < 800) {
                setOffsetX(0)
                setOffsetY(0)
            } else {
                setOffsetX(getX());
                setOffsetY(Math.random()*window.innerHeight/6 + window.innerHeight/8);
            }
            moveImage();
        }, 750)
    }

    useEffect(() => {
        const timer = moveImage()
        return () => clearTimeout(timer);
      }, []);

    return (
        <div className="main-banner">
            <div className="header">
                <div>
                    <h3><strong>Livecode.studio</strong></h3>
                </div>
                <div>
                    <h3><strong><a style={{textDecoration: 'none'}} href="#contact">Contact</a></strong></h3>
                </div>
            </div>
            <div className="banner-content">
                <div>
                    <h1>Performing With Code</h1>
                    <h4>Remote-Friendly Livecoding Course</h4>
                </div>
            </div>
            <div 
                className="banner-photo" 
                style={{
                    top: offsetY,
                    left: offsetX,
                }}
            >
                <video muted="true" preload="auto" poster="/static/photos/00_max_zach.gif" webkit-playsinline="true"  autoPlay="true" loop="loop" src="/static/photos/00_livecode_promo.mp4"/>
            </div>
            <Content class="subtext" full={true} disableSpacing={true}>
            <h2>Course Description</h2>
            <p>Performing With Code is a set of workshops showcasing four different approaches to the live performance of music and visuals using code. </p>
            <p>Taught by four friends and collaborators, participants will be introduced to two visual and two audio platforms and languages—Hydra, TidalCycles, GLSL, and ORCA—culminating in an Algorave for the participants to perform.</p>
            <p>Here is an example for a single <a href="http://charstiles.com/livecode/" target="_blank">2 hour workshop page</a>.</p>
            </Content>
        </div>
    )
}