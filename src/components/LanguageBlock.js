import React from 'react';
import './LanguageBlock.scss'

export function LanguageBlock(props) {
    return (
        <div className='language'>
            <div className='language-image-wrapper'>
            {
                props.link 
                ?
                <a href={props.link} target="_blank">
                    <div className='language-image'
                        style={{
                            backgroundImage: `url(${props.image})`
                        }}
                    >
                    </div>
                </a>
                :
                <div className='language-image'
                    style={{
                        backgroundImage: `url(${props.image})`
                    }}
                >
                </div>
            }
            </div>
            <div className='language-text'>
                {props.children}
            </div>
        </div>
    )
}